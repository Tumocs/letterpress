title: SIGCHI ja kuinka tuhota palvelin
date: 31/10/2018
excerpt: Tarina käytettävyydestä ja tärkeän datan ylläpidosta
tags: letterpress, SIGCHI, nextcloud, vps, itkk
lang: Finnish

---

Akateeminen leikkiminen hurahti käyntiin Ihminen Tietotekniikan Käyttäjänä ja Kehittäjänä kurssin muodossa ja ensimmäisenä toimenkuvana oli lurauttaa ilmoille blogi.
No.
Jääräpäänä käyttäjänä en tietenkään suostu käyttämään mitään suljettuja ratkaisuja, kömpelöitä blogikokonaisuuksia tai järisyttävän kryptisiä ratkaisuja. Näin ainoana vaihtoehtona jäi keksiä itse kuinka blogi tulisi toteuttaa. Onneksi taskusta löytyi VPS jossa jo on vuosikausia pyöritetty nextcloud palvelua henkilökohtaisen datan synkronointiin. Tästä innostuneena kiiruhdin opettelemaan html koodia ja kirjoittelemaan sitä ahkerasti.

Kaikki fiksuthan tietävät, että verkkosivujen tekeminen on helpoin asia ikinä.

Muutaman tunnin puurtamisen jälkeen totesin olleeni jokseenkin väärässä, ehkä maailmasta löytyy siedettäviä valmiihkoja ratkaisuja. Tovin jälkeen löytyi ratkaisuksi letterpress, joka näytti käytettävyytensä puolesta varsin simppeliltä. Torniosta löytyi myös sattumoisin kaveri joka lupasi auttaa kommenttien toteutuksen kanssa.

---

###"On suositeltavaa, että letterpress ajetaan juurikansiossa" 
sanottiin. Tehdään työtä käskettyä.

Todetaan että letterpress on poistanut kaikki alikansiot pilvipalvelua myöten.

Onneksi data oli tallessa (salattuna) toisaalla ja vain frontend on ryssitty. Palvelimen palauttaminen on siis todennäköisesti joskus kaukaisessa tulevaisuudessa mahdollista.

Kyseisestä kaiken pyyhkivästä ominaisuudesta ei tietenkään dokumentaatiossa ole mainittu mitään ja laiskana en tietysti jaksanut lukea lähdekoodiakaan läpi.

---

Näin pääsemme aasin siltaa pitkin käytettävyyteen ja SIGCHIn pariin. SIGCHI on Tietokone-Ihminen vuorovaikutuksesta kiinnostunut ryhmä. Se pyrkii kokoamaan yhteen ihmiset jotka haluavat tutkia, suunnitella tai kehittää ihmisen ja tietokonen välistä vuorovaikutusta ja käyttökokemusta.

SIGCHI Finland on järjestön suomalainen mustanaamiokerhon alajaosto, joka on tänä vuonna järjestämässä Oulussa World Usability Day (WUD) tapahtumaa.

Tämän vuoden aiheena on käyttäjäkokemussuunnitelu hyvän tai pahan vuoksi.
Olisi kovin mukavaa että käyttämäni blogialustan kehittäjä olisi kyseiseen tapahtumaan osallistunut koska nykyinen käyttäjäkokemuksen suunnittelu aiheutti ainakin hieman mielipahaa tälle käyttäjälle.
